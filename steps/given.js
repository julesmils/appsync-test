const velocityUtil = require("amplify-appsync-simulator/lib/velocity/util");

const an_appsync_context = (identity, args, result) => {
  // this simulates the $util helpers (https://docs.aws.amazon.com/appsync/latest/devguide/utility-helpers-in-util.html)
  const util = velocityUtil.create([], new Date(), Object());

  const context = {
    identity,
    args,
    arguments: args,
    result,
  };

  return {
    context,
    ctx: context,
    util,
    utils: util,
  };
};

module.exports = {
  an_appsync_context,
};
