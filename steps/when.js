const fs = require("fs");
const velocityMapper = require("amplify-appsync-simulator/lib/velocity/value-mapper/mapper");
const velocityTemplate = require("amplify-velocity-template");

const we_invoke_an_appsync_template = (templatePath, context) => {
  // load and parse the template file
  const template = fs.readFileSync(templatePath, { encoding: "utf-8" });
  const ast = velocityTemplate.parse(template);

  // map the "VST" java like syntax to javascript
  const compiler = new velocityTemplate.Compile(ast, {
    valueMapper: velocityMapper.map,
    escape: false,
  });

  // render with the given context
  return JSON.parse(compiler.render(context));
};

module.exports = {
  we_invoke_an_appsync_template,
};
