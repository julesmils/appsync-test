const given = require("../steps/given");
const when = require("../steps/when");
const path = require("path");

describe("Query.workouts.request template", () => {
  it("should generate batchGet a list of workouts", () => {
    const templatePath = path.resolve(
      __dirname,
      "../mapping-templates/Query.workouts.request.vtl"
    );

    const args = {
      names: ["2022 Highlights", "BODYATTACK #118 55 min"],
      programs: ["BONUS CONTENT", "BODYATTACK"],
    };

    const context = given.an_appsync_context({}, args);
    const result = when.we_invoke_an_appsync_template(templatePath, context);

    expect(result).toMatchObject({
      version: "2018-05-29",
      operation: "BatchGetItem",
      tables: {
        ["${DdbTableWorkouts}"]: {
          keys: expect.arrayContaining([
            expect.objectContaining({
              workoutName: {
                S: args.names[0],
              },
              programName: {
                S: args.programs[0],
              },
            }),
            expect.objectContaining({
              workoutName: {
                S: args.names[1],
              },
              programName: {
                S: args.programs[1],
              },
            }),
          ]),
        },
      },
    });
  });
});
